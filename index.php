<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<meta charset="utf-8">

	<!-- Primary Meta Tags -->
	<title>Elevator Direction — Independent elevator specialists you can trust</title>
	<meta name="title" content="Elevator Direction — Independent elevator specialists you can trust">
	<meta name="description" content="Our consultants bring with them knowledge developed over decades of working in the lift and escalator industry.">

	<meta property="og:type" content="website">
	<meta property="og:url" content="http://www.elevatordirection.com.au/">
	<meta property="og:title" content="Elevator Direction — Independent elevator specialists you can trust">
	<meta property="og:description" content="Our consultants bring with them knowledge developed over decades of working in the lift and escalator industry.">
	<meta property="og:image" content="img/ed-meta-image.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:url" content="http://www.elevatordirection.com.au/">
	<meta property="twitter:title" content="Elevator Direction — Independent elevator specialists you can trust">
	<meta property="twitter:description" content="Our consultants bring with them knowledge developed over decades of working in the lift and escalator industry.">
	<meta property="twitter:image" content="img/ed-meta-image.png">

	<?php include_once('includes/header.php'); ?>

</head>

<body>
	<?php include_once('includes/nav.php'); ?>

	<div class="banner banner--main">
		<div class="container">
			<h1>Independent elevator specialists you can trust</h1>
		</div>
	</div>
	
	<section class="container page-home page-home--services">
		<h2>Our Services</h2>
		<p class="lead">Elevator Direction's consultants bring with them knowledge developed over decades of working in the lift and escalator
			industry.
		</p>
		<div class="row services-items-wrapper">
			<div class="col-xs-12 col-sm-6 col-md-4">
				<a class="service-item" href="services.php#service-vertical-transporation">
					<i class="icon-service">
						<img src="img/icon-service-vertical-transportation-design.svg" />
					</i>
					<span>Vertical Transportation Design</span>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<a class="service-item" href="services.php#technical-specification">
						<i class="icon-service">
							<img src="img/icon-service-technical-specifications.svg" />
						</i>
					<span>Technical Specifications</span>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<a class="service-item" href="services.php#service-tendering">
					<i class="icon-service">
						<img src="img/icon-service-tendering-and-assessment.svg" />
					</i>
					<span>Tendering & Assessment</span>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<a class="service-item" href="services.php#service-project-management">
					<i class="icon-service">
						<img src="img/icon-service-project-management.svg" />
					</i>
					<span>Project Management</span>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<a class="service-item" href="services.php#service-condition-and-modernisation">
					<i class="icon-service">
						<img src="img/icon-service-condition-reports.svg" />
					</i>
					<span>Condition Reports & Modernisation</span>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<a class="service-item" href="services.php#service-annual-audit">
					<i class="icon-service">
						<img src="img/icon-service-annual-audit.svg" />
					</i>
					<span>Annual Audit Inspections Maintenance Agreements</span>
				</a>
			</div>
		</div>
	</section>

	<section class="container-fluid container--brand page-home page-home--case-studies">
		<h2 class="--on-dark">Case Studies</h2>
		<p class="--on-dark">Technical knowledge and compliance to all relevant legislation is fundamental to our services provided. We ensure our clients
			are supplied with correct information the first time.</p>

		<div class="row case-studies-wrapper">
			<a class="case-study-item" href="case-studies.php#cs-escalator-replacement">
				<img src="img/case-studies/case-study-img4.jpg" />
				<div class="case-study-info">
					<span>Case study #1</span>
					<h5 class="--on-dark">Escalator Replacement</h5>
				</div>
			</a>
			<a class="case-study-item" href="case-studies.php#cs-more-nla">
				<img src="img/case-studies/case-study-img1.jpg" />
				<div class="case-study-info">
					<span>Case study #2</span>
					<h5 class="--on-dark">More NLA Up & Down</h5>
				</div>
			</a>
			<a class="case-study-item" href="case-studies.php#cs-life-cycle">
				<img src="img/case-studies/case-study-img2.jpg" />
				<div class="case-study-info">
					<span>Case study #3</span>
					<h5 class="--on-dark">Life Cycle</h5>
				</div>
			</a>
			<a class="case-study-item" href="case-studies.php#cs-public-rail-works">
				<img src="img/case-studies/case-study-img3.jpg" />
				<div class="case-study-info">
					<span>Case study #4</span>
					<h5 class="--on-dark">Public Rail Works</h5>
				</div>
			</a>
		</div>

		<a class="button button--default button-lg button--secondary button--consult" href="contact.php">Consult with us for your next project</a>
	</section>

	<section class="container page-home page-home--testimonials">
		<h2>Testimonials</h2>
		<div class="row align-items-center">
			<div class="col-sm-1 col-md-2"></div>
			<div class="col-xs-12 col-sm-10 col-md-8">
				<div class="testimonials">
					<div class="testimonials--item" data-time="3000">
						<blockquote class="lead">
							<span class="muted">Residential</span>
							<p>Elevator Direction’s services to the Strata Industry are essential to us as Strata Managers. They understand the lift industry and are able to communicate at a technical level to Lift Contractors, on behalf of the Strata Company.</p>
							<br />
							<p>Their ability to provide clear reporting, budgeting and quality documentation takes the risk away from the Strata Company, while providing the right, and most cost effective, solution for lifts owned by the Strata Schemes we manage.</p>
							<br />
							<p>We recommend using Elevator Direction for lift inspections, replacements, tendering and project management.</p>
							<footer class="blockquote-footer">
								<span class="blockquote-footer--person">Beverly Grigo</span>
								<span class="blockquote-footer--title">Director – Strata Asset Services (WA) PTY</span>
								<span class="blockquote-footer--title">Certified Level 4 Strata Community Manager (CSCM)</span>
								<span class="blockquote-footer--title">Life Member and Fellow (FSCM)</span>
							</footer>
						</blockquote>
					</div>
					<div class="testimonials--item" data-time="5000">
						<blockquote class="lead">
							<span class="muted">Commercial</span>
							<p>We rely on Elevator Direction’s extensive knowledge of the lift and escalator requirements that large commercial buildings have.</p>
							<br />
							<p>Their understanding of the lift code, individual lift Contractor’s products and capabilities make the tender and decision making process simple.</p>
							<br />
							<p>Elevator Direction are able to assemble and manage the right team of people to ensure a smoothly finished project without any unforeseen additional costs.</p>
							<br /><p>From initial design through to final commissioning, Elevator Direction can be relied on to provide a regular efficient lift and escalator consulting service.</p>
							<footer class="blockquote-footer">
								<span class="blockquote-footer--person">Tony Morgan</span>
								<span class="blockquote-footer--title">General Manager</span>
								<span class="blockquote-footer--title">Westralia Square</span>
							</footer>
						</blockquote>
					</div>
					<div class="testimonials--item" data-time="5000">
						<blockquote class="lead">
							<span class="muted">Industrial</span>
							<p>The partnership between Elevator Direction and CBH has greatly improved on the maintenance schedules of CBH lifts.</p>
							<br />
							<p>Modernisation programs and upgrades have been implemented under Elevator Direction project management. These projects have been completed within allocated budgets. </p>
							<br />
							<p>As such CBH lifts are now maintained to current industry standtmeards through the guidance of Elevator Direction.</p>
							<footer class="blockquote-footer">
								<span class="blockquote-footer--person">John Cairns</span>
								<span class="blockquote-footer--title">CBH Group</span>
								<span class="blockquote-footer--title">N&E Innovation and Technology Manager</span>
							</footer>
						</blockquote>
					</div>
				</div>
			</div>
		</div>
	</section>

	
	<?php include_once('includes/footer.php'); ?>
	<!-- Javascript
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<?php include_once('includes/js.php'); ?>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			$('.testimonials').slick({
				infinite: true,
				slidesToShow: 1,
				autoplay: true,
				autoplaySpeed: 5000,
				arrows: true,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							arrows: false,
							dots: true,
							adaptiveHeight: true
						}
					}
				]
			}).on("afterChange", function (e, slick) {
				//add data-time attribute for longer slide items, ie. data-time="4000"
				slideTime = $('div[data-slick-index="' + slick.currentSlide + '"]').data("time");
				$('.testimonials').slick("setOption", "autoplaySpeed", slideTime);
			});
		});
	</script>
</body>

</html>