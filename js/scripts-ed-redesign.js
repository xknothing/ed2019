$(function () {
	"use strict";

	var headerMain = $('#header-main'),
			navToggle = $('#nav-toggle'),
			navItems = $('.header-main__site-nav-items');

	//trigger responsive nav
	navToggle.on('click', function () {
		if (headerMain.hasClass('--active')) {
			headerMain.removeClass('--active');
		} else {
			headerMain.addClass('--active');
		}
	});
	
	$('#menu-main .button--close').on('click', function(){
		headerMain.removeClass('--active');
	})

	//close responsive nav on click outside menu
	$('html').on('click', function(e) {
		if ( event.target.closest('.header-main__site-nav-items') && headerMain.hasClass('--active') ) return;
		else if ( !$(e.target).hasClass('nav-toggle') ) headerMain.removeClass('--active');
	});

	
	
	
});