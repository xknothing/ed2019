'use strict';

var fields = {
    'cd-name': {
        regex: /^\s*([A-Za-z]{1,}([\.,] |[-']| ))+[A-Za-z]+\.?\s*$/,
        error: 'Please enter valid name.'
    },
    'cd-email': {
        regex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        error: 'Please enter valid email address.'
    }
}

function recaptchaCallback() {
    $('#submit-btn').removeAttr('disabled');
};

$(document).ready(function () {
    var form = $('#contact-form');

    if (!form.length) {
        return;
    }

    form.on('submit', function (e) {
        e.preventDefault();
        $('.error-message, .success-message').empty();
        var data = {};

        $(this).find('[name]').each(function () {
            var name = $(this).attr('name');
            var value = $(this).val();
            var field = fields[name];

            if (field && field.regex && !field.regex.test(value)) {
                $('.error-message').append('<p>' + field.error + '</p>');
            }

            data[name] = value;
        });

        if ($('.error-message > p').length) {
            return;
        }

        $.ajax({
            url: 'mailer.php',
            method: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                $('.button--contact-submit').addClass('loading');
            },
            complete: function () {
                $('.button--contact-submit').removeClass('loading');
            },
            success: function (response) {
                if (response.error) {
                    $('.error-message').append('<p>' + response.message + '</p>');
                } else {
                    $('.success-message').append('<p>' + response.message + '</p>');
                    form.trigger('reset');
                }

                grecaptcha && grecaptcha.reset();
                $('#submit-btn').attr('disabled', true);
            }
        });
    });
});