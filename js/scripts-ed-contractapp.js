$(function(){
    "use strict";

		//initialize tooltips 
		var tooltips = $('[data-toggle="tooltip"]');
		if ( tooltips.length > 0 ) {
			$(tooltips).tooltip();
		}
		
		//'tooltip' showing for input fields on focus
    $('.form-control').popover({
        trigger: 'focus'
		});

		//set first input fields to autofocus
		$('form:first input[type!=hidden]:first').focus();
		
		//datepicker field
		if ( $('.gijgo-datepicker').length > 0 ) {
			$('.gijgo-datepicker').datepicker({
				uiLibrary: 'bootstrap4',
				iconsLibrary: 'fontawesome',
				format: 'dd/mm/yyyy',
				minDate: 'today',
				size: 'large'
			});
		}

		//modal onload
		var loginsignup = $('[data-target="#modal-ED-login"], [data-target="#modal-ED-signup]');
		if ( loginsignup.length > 0 ) {
			loginsignup.trigger('click');
		}

    // Example starter JavaScript for disabling form submissions if there are invalid fields
    window.addEventListener('load', function() {
			// Fetch all the forms we want to apply custom Bootstrap validation styles to
			var forms = document.getElementsByClassName('needs-validation');

			// Loop over them and prevent submission
			var validation = Array.prototype.filter.call(forms, function(form) {
				form.addEventListener('submit', function(event) {
					if (form.checkValidity() === false) {
						event.preventDefault();
						event.stopPropagation();
					}
					form.classList.add('was-validated');
				}, false);
			});
		}, false);
    
});