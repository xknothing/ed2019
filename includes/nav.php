<header id="header-main" class="container--fluid header-main">
  <div class="header-main__contact">
    <a class="" href="tel:0406 676 036">
      <i class="icon-ed icon-ed--call">
        <svg viewBox="0 0 110 110" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="icon-call" fill="#000000" fill-rule="nonzero">
              <path d="M80.6666667,66 C73.3333333,73.3333333 73.3333333,80.6666667 66,80.6666667 C58.6666667,80.6666667 51.3333333,73.3333333 44,66 C36.6666667,58.6666667 29.3333333,51.3333333 29.3333333,44 C29.3333333,36.6666667 36.6666667,36.6666667 44,29.3333333 C51.3333333,22 29.3333333,0 22,0 C14.6666667,0 0,22 0,22 C0,36.6666667 15.0663333,66.3996667 29.3333333,80.6666667 C43.6003333,94.9336667 73.3333333,110 88,110 C88,110 110,95.3333333 110,88 C110,80.6666667 88,58.6666667 80.6666667,66 Z"
                id="Shape"></path>
            </g>
          </g>
        </svg>
        </i>Call us 0406 676 036
    </a>
  </div>
  <div class="header-main__site-nav">
    <a class="header-main__site-logo" href="index.php">
      <img src="img/logo-ed.png" class="d-inline-block align-top" alt="Elevator Direction logo" />
    </a>
    <div class="header-main__nav-toggle">
      <!-- <input type="checkbox" id="nav-toggle" class="hidden-lg"> -->
      <input type="button" id="nav-toggle" class="nav-toggle hidden-lg"></button>
      <span></span>
      <span></span>
      <span></span>
    </div>
    <nav id="menu-main" class="header-main__site-nav-items" role="navigation" class="okayNav">
      <button class="button button--close">X Close</button>
      <ul>
        <li>
          <a href="index.php">Home</a>
        </li>
        <li>
          <a href="about.php">About Us</a>
        </li>
        <li>
          <a href="services.php">Services</a>
        </li>
        <li>
          <a href="case-studies.php">Case Studies</a>
        </li>
        <li>
          <a href="contact.php">Contact Us</a>
        </li>
        <!-- <li>
          <a href="#" class="button button--default button--small">Create a contract</a>
        </li> -->
      </ul>
    </nav>
  </div>
</header>
