<footer id="footer">
  <?php if (!isset($page) || isset($page) && $page != 'contact') { ?>
  <div class="container footer-navigation">
    <div class="row">
      <div class="col-sm-12">
        <h2>Get in touch</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 col-md-8">
        <dl>
          <dt>Western Australia</dt>
          <dd>
            <span>Office: Level 3 | 28 The Esplanade | Perth WA 6000</span>
          </dd>
          <dd>
            <span>Mail:  PO Box Z5510 | St Georges Terrace WA 6831</span>
          </dd>
          <dd>
            <span><a href="tel:+614-0667-6036" class="footer-link">Phone:  0406 676 036</a></span>
          </dd>
        </dl>
        <dl>
          <dt>South Australia</dt>
          <dd>
            <span>Mail: PO Box 228 | Henley Beach SA 5022</span>
          </dd>
          <dd>
            <span><a href="tel:0431 861 999" class="footer-link">Phone:  0431 861 999</a></span>
          </dd>
        </dl>
      </div>
      <div class="col-sm-12 col-md-4">
        <dl>
          <dd>
            <ul class="footer-contact">
                <li>
                  <a href="https://www.linkedin.com/in/john-schifferli-28a0155" class="footer-link" target="_blank">
                    <i class="icon-ed icon-ed--call">
                      <svg viewBox="0 0 119 110" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <defs></defs>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <g id="icon-linkedin" fill="#000000" fill-rule="nonzero">
                            <path d="M42.5,33.8461538 L66.02375,33.8461538 L66.02375,45.8488462 L66.3595,45.8488462 C69.632,40.0061538 77.64325,33.8461538 89.5815,33.8461538 C114.41,33.8461538 119,49.2334615 119,69.245 L119,110 L94.48175,110 L94.48175,73.8692308 C94.48175,65.2511538 94.30325,54.1665385 81.7275,54.1665385 C68.95625,54.1665385 67.00975,63.5503846 67.00975,73.2430769 L67.00975,110 L42.5,110 L42.5,33.8461538 Z"
                            id="Shape"></path>
                            <polygon id="Shape" points="0 33.8461538 25.5 33.8461538 25.5 110 0 110"></polygon>
                            <path d="M25.5,12.6923077 C25.5,19.7026923 19.79225,25.3846154 12.75,25.3846154 C5.70775,25.3846154 0,19.7026923 0,12.6923077 C0,5.68192308 5.70775,0 12.75,0 C19.79225,0 25.5,5.68192308 25.5,12.6923077 Z"
                            id="Shape"></path>
                          </g>
                        </g>
                      </svg>
                    </i>
                    John Schifferli (LinkedIn)
                  </a>
                </li>
                <li>
                  <hr class="footer-divider">
                </li>
                <li>
                  <dl>
                    <dt>ABN: 76 157 388 398</dt>
                  </dl>
                </li>
              </ul>
          </dd>
        </dl>
      </div>
    </div>
  </div>
  <?php } ?>
  
  <div class="footer-bottom">
    &copy; <?= date("Y"); ?> Copyright Elevator Direction
  </div>
</footer>