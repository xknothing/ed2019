<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<meta charset="utf-8">
	
	<!-- Primary Meta Tags -->
	<title>Case Studies — Elevator Direction</title>
	<meta name="title" content="Elevator Direction — Case Studies">
	<meta name="description" content="We believe in offering professional, efficient and ethical elevator consulting services to our clients.">

	<meta property="og:type" content="website">
	<meta property="og:url" content="http://www.elevatordirection.com.au/case-studies.php">
	<meta property="og:title" content="Elevator Direction — Case Studies">
	<meta property="og:description" content="We provide a wide range of consulting services for buildings. We can assist with service audits and compliances, upgrades and modernization or full lift replacements assessments.">
	<meta property="og:image" content="img/ed-meta-image.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:url" content="http://www.elevatordirection.com.au/case-studies.php">
	<meta property="twitter:title" content="Elevator Direction — Case Studies">
	<meta property="twitter:description" content="We provide a wide range of consulting services for buildings. We can assist with service audits and compliances, upgrades and modernization or full lift replacements assessments.">
	<meta property="twitter:image" content="img/ed-meta-image.png">

	<?php include_once('includes/header.php'); ?>

</head>

<body>
	<?php include_once('includes/nav.php'); ?>

	<div class="container-fluid page-inner page-inner--case-studies">
		<div class="page-inner__page-header">
			<h1>Case Studies</h1>
		</div>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 page-inner-case-study-list">
				<article class="case-study__item" id="cs-escalator-replacement">
					<div class="case-study__item-image">
					<img src="img/case-studies/case-study-img4.jpg" alt="Case Study - Escalator Replacement" />
					</div>
					<div class="case-study__item-desc">
						<span class="case-study__label">Case Study #1</span>
						<h4>Escalator Replacement</h4>
						<p>
							In any project getting the right team together upfront, is the most important step.
						</p>
						<p>
							When the Adelaide Central Market Authority (ACMA) decided they needed to upgrade their escalators, they wanted to be sure that this high profile project could be completed with confidence.
						</p>
						<p>
							Elevator Direction combined the skills of ACMA's existing skilled Engineers, a Builder and a Lift Contractor to produce the design and tender documents for the replacement of the four core escalators.
						</p>
						<p>
							In such a public and iconic place at the very the heart of Adelaide City, careful planning and communication were critical.
						</p>
						<p>
							Elevator Direction understood the need to have safe control of a potentially hazardous space, where large logistically challenging pieces of escalator had to be lifted and traversed through an operating market. This was done by ensuring a Head Contractor took ownership of the site and public safety.
						</p>
						<p>
						Longevity in design is key to the operation of quality sites. Because Elevator Direction performs annual compliance inspections, we understand the benefit of a quality product in the long term, and, with permission from our client, we're able to specify quality escalator products.
						</p>
					</div>
				</article>
				<article class="case-study__item" id="cs-more-nla">
					<div class="case-study__item-image">
					<img src="img/case-studies/case-study-img1.jpg" alt="Case Study - More NLA Up & Down" />
					</div>
					<div class="case-study__item-desc">
						<span class="case-study__label">Case Study #2</span>
						<h4>More NLA Up & Down</h4>
						<p>Modernising an existing fully occupied 31 story dual lift bank building has its complexities, but when the Owners decided to maximise their lettable space by exploring whether lifts could access the floor higher and the floor lower, the project got interesting.
						</p>
						<p>Elevator Direction, along with a leading Lift Company, Structural Engineer, Geo Technical Engineer and Registered Builder set to work to see what was achievable.</p>
						<p>Critical to the Concept Design was allowing two lifts of the five high rise bank of lifts to access the previously unoccupied Level 31 and allowing two lifts, one from each rise, to answer the level below the existing lowest floor, for the newly proposed basement food court.
						</p>
						<p>
								Other factors included ensuring one lift from each rise had 2,000 mm deep stretcher capacity cars, that the building canopy wasn’t exceeded and that the necessary Council Building Approvals were granted.
						</p>
						<p>Four years after the Concept Design was completed the building project renovations were finished and had lifts and escalators had undergone complete transformation. 
						</p>
						<p>Lift access to Level 31 created a further 800 sqm of net lettable area for the owner, fully tenanted. The building having increased in size also increased in value when re-valued on completion.
						</p>
						<p>
								Sinking two lifts down to the basement, although requiring extensive excavations, intense building works and dealing with subterranean water, allowed building tenants a single lift ride from their floors to the food court below.
						</p>
						<p>
								Access to all floors was now provided by efficient lift Destination Control Technology and high level security.
						</p>
						<p>
								A thorough Concept Design and Technical Specification allows for a quality finish where all parties concerned received positive outcomes.
						</p>
					</div>
				</article>
				<article class="case-study__item" id="cs-life-cycle">
						<div class="case-study__item-image">
						<img src="img/case-studies/case-study-img2.jpg" alt="Case Study - Life Cycle" />
						</div>
						<div class="case-study__item-desc">
							<span class="case-study__label">Case Study #3</span>
							<h4>Life Cycle</h4>
							<p>A major University approached Elevator Direction with the intent to drive longer term maintenance costs down, to ensure code compliance, disability compliance and improved safety across their 50 lift portfolio.</p>
							<p>Elevator Direction started by completing an annual lift inspection with a dedication section to lift life cycle. </p>
							<p>This included a 10 year plan with budget costs and a sequence of recommended lift replacements based on obsolescence, technology and safety.
							</p>
							<p>Elevator Direction completed individual site specific technical specifications for tender on behalf of the University to get the capital replacement program underway.</p>
							<p>Within five years of this process starting, Elevator Direction showed that the problematic lifts which continued to fail in the past, once replaced, significantly improved the breakdown rate of the whole portfolio.</p>
							<p>A secondary tendering process was undertaken where lift companies were encouraged to provide a performance based approach to maintenance program rather than a traditional full time site presence. </p>
							<p>What resulted was an improved maintenance saving to the University and a service that actually improved due to the implemented life cycle replacement works program.</p>
							<p>A reliable, modern compliant lift system provides many saving including public satisfaction, less administration, less power usage and removes obsolescence.</p>
							
						</div>
				</article>
				<article class="case-study__item" id="cs-public-rail-works">
						<div class="case-study__item-image">
							<img src="img/case-studies/case-study-img3.jpg" alt="Case Study - Public Rail Works" />
						</div>
						<div class="case-study__item-desc">
							<span class="case-study__label">Case Study #4</span>
							<h4>Public Rail Works</h4>
							<p>As soon as the design process starts for Public works, the requirement for a higher level of quality and robustness of product compared to typical commercial usage becomes evident.</p>
							<p>Public infrastructure and assets must have minimal long term failures, provision for redundancy and a high level of safety in design. </p>
							<p>Elevator Direction were engaged to work through many documents and stakeholders in order to provide the right consultative design for stations, tunnels and other Vertical transportation facilities.</p>
							<p>Key to proving these services was to work closely with the multidiscipline team that provides services related to VT such mechanical, structural, architectural, electrical, hydraulic, security and communication.</p>
							<p>Through consultation and communication Elevator Direction were able to provide approved documentation to be priced by appropriate Contractors.</p>
						</div>
				</article>
			</div>
			<div class="col-md-3"></div>
		</div>
		
		
			
		
	</div>

	<?php include_once('includes/footer.php'); ?>

	<!-- Javascript
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<?php include_once('includes/js.php'); ?>
</body>

</html>