<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<meta charset="utf-8">
	
	<!-- Primary Meta Tags -->
	<title>Contact Us — Elevator Direction</title>
	<meta name="title" content="Elevator Direction — Contact Us">
	<meta name="description" content="">

	<meta property="og:type" content="website">
	<meta property="og:url" content="http://www.elevatordirection.com.au/contact.php">
	<meta property="og:title" content="Elevator Direction — Contact Us">
	<meta property="og:description" content="">
	<meta property="og:image" content="img/ed-meta-image.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:url" content="http://www.elevatordirection.com.au/contact.php">
	<meta property="twitter:title" content="Elevator Direction — Contact Us">
	<meta property="twitter:description" content="">
	<meta property="twitter:image" content="img/ed-meta-image.png">

	<?php include_once('includes/header.php'); ?>

</head>

<body>
<?php include_once('includes/nav.php'); ?>
	<div class="container-fluid page-inner page-inner--contact">
		<div class="page-inner__page-header">
			<h1>Contact Us</h1>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<div class="contact-item-wrapper">
					<div class="contact-item__desc">
						<dl>
							<dt><h4>Western Australia</h4></dt>
							<dd>
								<span>Office: Level 3 | BGC Centre | 28 The Esplanade | Perth WA 6000</span>
							</dd>
							<dd>
								<span>Mail:  PO Box Z5510 | St Georges Terrace WA 6831</span>
							</dd>
						</dl>
					</div>
					<div class="contact-item__desc">
						<dl>
							<dt><h4>South Australia</h4></dt>
							<dd>
								<span>Mail: PO Box 228 | Henley Beach SA 5022</span>
							</dd>
						</dl>
					</div>
					<div class="responsive-map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3385.173121290306!2d115.85489261524006!3d-31.956199781227653!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2a32bdbe572c6397%3A0x74eee6173e8ad523!2sElevator+Direction+Lift+Consultant!5e0!3m2!1sen!2sph!4v1554646496243!5m2!1sen!2sph" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
				</div>
			</div>
			<div class="col-md-5">
					<div class="contact-form-wrapper">
							<form id="contact-form" class="cd-form floating-labels">
								<fieldset>
										<legend>We'd love to hear from you!<span>Feel free to send us a message and we'll get back to you as soon as possible.</span></legend>
							
										<div class="icon">
											<label class="cd-label" for="cd-name">Full Name</label>
											<input class="user" type="text" name="cd-name" id="cd-name" required />
										</div> 
		
										<div class="icon">
										<label class="cd-label" for="cd-email">Email</label>
										<input class="email" type="email" name="cd-email" id="cd-email" required />
										<!-- <input class="email error" type="email" name="cd-email" id="cd-email" required=""> -->
									</div>
		
									<div class="icon">
										<label class="cd-label" for="cd-textarea">Message</label>
										<textarea class="message" name="cd-textarea" id="cd-textarea" required></textarea>
									</div>
						
									<div>
										<div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6Ld6IZ4UAAAAAFssCyPNsQFL6y7aLA5nP1hSgOnL"></div>
										<button id="submit-btn" type="submit" class="button button--default button--contact-submit" disabled>Send Message<div class="spin-loader" aria-hidden="true"></div></button>
									</div>

									<div>
										<div class="error-message"></div>
										<div class="success-message"></div>
									</div>
							
										<!-- ... -->
								</fieldset>
							
							</form>
						</div>
			</div>

			<div class="col-md-1"></div>
		</div>
		
		
	</div>
	<?php $page = 'contact' ; include_once('includes/footer.php'); ?>
	<!-- Javascript
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<?php include_once('includes/js.php'); ?>
	
</body>

</html>