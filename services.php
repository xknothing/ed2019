<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<meta charset="utf-8">
	
	<!-- Primary Meta Tags -->
	<title>Our Services — Elevator Direction</title>
	<meta name="title" content="Elevator Direction — Services">
	<meta name="description" content="We provide a wide range of consulting services for buildings. We can assist with service audits and compliances, upgrades and modernisation or full lift replacements assessments.">

	<meta property="og:type" content="website">
	<meta property="og:url" content="http://www.elevatordirection.com.au/services.php">
	<meta property="og:title" content="Elevator Direction — Services">
	<meta property="og:description" content="We provide a wide range of consulting services for buildings. We can assist with service audits and compliances, upgrades and modernisation or full lift replacements assessments.">
	<meta property="og:image" content="img/ed-meta-image.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:url" content="http://www.elevatordirection.com.au/services.php">
	<meta property="twitter:title" content="Elevator Direction — Services">
	<meta property="twitter:description" content="We provide a wide range of consulting services for buildings. We can assist with service audits and compliances, upgrades and modernisation or full lift replacements assessments.">
	<meta property="twitter:image" content="img/ed-meta-image.png">

	<?php include_once('includes/header.php'); ?>

</head>

<body>
	<?php include_once('includes/nav.php'); ?>

	<div class="container-fluid page-inner page-inner--services">
		<div class="page-inner__page-header">
			<h1>Services</h1>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<p class="lead">Elevator Direction provides a wide range of consulting services for buildings. We can assist with service audits and
					compliances, upgrades and modernisation or full lift replacements assessments.
				</p>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 page-inner-services-list">
				<div class="services-list__item" id="service-vertical-transporation">
					<div class="services-list__item-image" style="background-image:url('https://images.unsplash.com/photo-1518014439787-129d1148ede5?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=9c159695125bd6b58c79fbd3b4200027&auto=format&fit=crop&w=2089&q=80')">
					</div>
					<div class="services-list__item-desc">
						<h4>Vertical Transportation Design</h4>
						<p>At the start on any project we are able to provide concept and detail design while working as standalone consultants or as part of multidiscipline team.</p>
						<p>Our skills include provision of traffic studies, lift shaft sizing, escalator detail and providing industry product knowledge from multiple suppliers. Our designs are completed with reference to regulatory authorities including Australian Standards, Building Code of Australia (NCC) and the Property Council of Australia Guidelines.</p>
					</div>
				</div>
				<div class="services-list__item" id="technical-specification">
					<div class="services-list__item-image" style="background-image:url('https://images.unsplash.com/photo-1489515229412-1f3a8f08dc34?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=e71a7bf83a2ee0f744cf11b0554554f4&auto=format&fit=crop&w=1650&q=80')">
					</div>
					<div class="services-list__item-desc">
						<h4>Technical Specifications </h4>
						<p>We specialise in writing individualised specifications for any given project. Our objectives are to provide long term reliability, quality, compliance and pleasing aesthetics on all our lift projects. We provide shaft sizing, lift speeds, number of lifts required at the design stage of new projects.</p>
						<p>This information is critical right up front as lift pits are always the first parts of a building to be constructed.</p>
					</div>
				</div>
				<div class="services-list__item" id="service-tendering">
					<div class="services-list__item-image" style="background-image:url('https://images.unsplash.com/photo-1540454250141-3501e164b712?ixlib=rb-0.3.5&s=53ee5f398c344973ca00f5e5c6ebd05c&auto=format&fit=crop&w=1650&q=80')">
					</div>
					<div class="services-list__item-desc">
						<h4>Tendering &amp; Valued Engineering</h4>
						<p>We combine technical specifications and commercial terms for formal tendering on our clients behalf. We believe the real value we provide our clients is competitive pricing to the right scope of work. We assess returned tenders, provide valued engineering, and make recommendations for value for money.</p>
					</div>
				</div>
				<div class="services-list__item" id="service-project-management">
					<div class="services-list__item-image" style="background-image:url('https://images.unsplash.com/photo-1527755018186-56a6bfb32678?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a9c680ae8366771cfd52adae148cc0c5&auto=format&fit=crop&w=1651&q=80')">
					</div>
					<div class="services-list__item-desc">
						<h4>Project Management</h4>
						<p>We provide contract management to ensure projects are delivered in a timely manner and without additional variation costs.</p>
					</div>
				</div>
				<div class="services-list__item" id="service-condition-and-modernisation">
					<div class="services-list__item-image" style="background-image:url('https://images.unsplash.com/photo-1523430045879-9444a84c28eb?ixlib=rb-0.3.5&s=7df462aff5b11eb7cd021add23ffd999&auto=format&fit=crop&w=1650&q=80')">
					</div>
					<div class="services-list__item-desc">
						<h4>Condition &amp; Modernisation</h4>
						<p>We provide life cycle reports, scope with budget pricing for lift replacements. Our modernisation specifications are designed to retain products that still have value and yet replace componentry where to meet code compliance obsolescence and budgets.</p>
					</div>
				</div>
				<div class="services-list__item" id="service-annual-audit">
					<div class="services-list__item-image" style="background-image:url('https://images.unsplash.com/photo-1534398079543-7ae6d016b86a?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=72890f69ff88dcdcd9df40fee16eca4d&auto=format&fit=crop&w=1650&q=80')">
					</div>
					<div class="services-list__item-desc">
						<h4>Annual Audit Inspections Maintenance Agreements</h4>
						<p>We provide site inspections and analysis on lift and escalator performance. These results are benchmarked against the industry averages, the age of the equipment, codes and standards. We provide tailored maintenance agreements for our clients, noting their best interests in fair manner.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>


	</div>

	<?php include_once('includes/footer.php'); ?>

	<!-- Javascript
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<?php include_once('includes/js.php'); ?>
</body>

</html>