<?php

require './vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

define('SMTP_AUTH_USERNAME', 'elevatordirection2019@gmail.com');
define('SMTP_AUTH_PASSWORD', 'passED2019');
define('RECAPTACHA_SECRET_KEY', '6Ld6IZ4UAAAAANoAvGYtGmkD8lqT4n7RNZ9zWXFW');
// define('MAIL_TO', 'info@elevatordirection.com.au');
define('MAIL_TO', 'elevatordirection2019@gmail.com');

$fields = array(
    'name' => 'cd-name',
    'from' => 'cd-email',
    'message' => 'cd-textarea'
);

function check_captcha($captcha) {
    $ip = $_SERVER['REMOTE_ADDR'];					
	$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . RECAPTACHA_SECRET_KEY . "&response=" . $captcha . "&remoteip=" . $ip);
	$responseKeys = json_decode($response,true);	     

    return intval($responseKeys["success"]) === 1;
}

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $err = false;
    $msg = '';
    $query = '';
    $from = '';
    $name = '';

    //Apply some basic validation and filtering to the query
    if (array_key_exists($fields['message'], $_POST)) {
        //Limit length and strip HTML tags
        $query = substr(strip_tags($_POST[$fields['message']]), 0, 16384);
    } 
    //Apply some basic validation and filtering to the name
    if (array_key_exists($fields['name'], $_POST)) {
        //Limit length and strip HTML tags
        $name = substr(strip_tags($_POST[$fields['name']]), 0, 255);
    }

    if (array_key_exists($fields['from'], $_POST) && PHPMailer::validateAddress($_POST[$fields['from']])) {
        $from = $_POST[$fields['from']];
    } else {
        $msg .= 'Error: invalid email address provided';
        $err = true;
    }

    if (!check_captcha($_POST['g-recaptcha-response'])) {
        $msg .= 'Error: invalid captcha provided';
        $err = true;
    }

    $response = array(
        'error' => $err,
        'message' => $msg
    );

    if (!$err) {
        try {
            $whitelist = array(
                '127.0.0.1',
                '::1'
            );
    
            $mail = new PHPMailer(); // create a new object

            // Check if localhost
            if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
                $mail->IsSMTP(); // enable SMTP
                // $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
                $mail->SMTPAuth = true; // authentication enabled
                $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
                $mail->Host = 'smtp.gmail.com';
                $mail->Port = 587; // 465 for SSL and 587 for TLS
                $mail->Username = SMTP_AUTH_USERNAME;
                $mail->Password = SMTP_AUTH_PASSWORD;
            } 
        
            $mail->isHTML(true);

            $mail->CharSet = 'utf-8';
            $mail->setFrom($from, $name);
            // $mail->setFrom($from);
            $mail->addAddress(MAIL_TO);
            // $mail->addReplyTo($from);
            $mail->addReplyTo($from, $name);
            $mail->Subject = 'Inquiry via contact form';
            $mail->Body = $query . '<br /><br /> - ' . $name;

            if (!$mail->send()) {
                $err = true;
                $msg = 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                $msg = 'Thank you, your message has been sent!';
            }
        } catch (Exception $e) {
            $err = true;
            $msg = $e->getMessage();
        }
    } 

    echo json_encode(array(
        'error' => $err,
        'message' => $msg
    ));
} else {
    header("Location: contact.php");
    die();
}