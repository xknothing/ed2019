<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<meta charset="utf-8">

	<!-- Primary Meta Tags -->
	<title>About Us — Elevator Direction</title>
	<meta name="title" content="Elevator Direction — About Us">
	<meta name="description" content="We believe in offering professional, efficient and ethical elevator consulting services to our clients.">

	<meta property="og:type" content="website">
	<meta property="og:url" content="http://www.elevatordirection.com.au/about.php">
	<meta property="og:title" content="Elevator Direction — About Us">
	<meta property="og:description" content="We believe in offering professional, efficient and ethical elevator consulting services to our clients.">
	<meta property="og:image" content="img/ed-meta-image.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:url" content="http://www.elevatordirection.com.au/about.php">
	<meta property="twitter:title" content="Elevator Direction — About Us">
	<meta property="twitter:description" content="We believe in offering professional, efficient and ethical elevator consulting services to our clients.">
	<meta property="twitter:image" content="img/ed-meta-image.png">

	<?php include_once('includes/header.php'); ?>

</head>

<body>
<?php include_once('includes/nav.php'); ?>
	<div class="container-fluid page-inner page-inner--about">
		<div class="page-inner__page-header">
			<h1>About Us</h1>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<p class="lead">Elevator Direction was established in 2010 and offers lift consulting services throughout Australia.</p>
				<p>We believe in offering professional, efficient and ethical elevator consulting services to our clients. Technical knowledge and compliance to all relevant legislation is fundamental to our services provided.</p>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-xs-12 col-md-8">
				<h2 class="about__location-header"><span>Western Australia</span></h2>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 page-inner-about-list">
				<div class="about__item">
					<div class="about__item-desc">
						<h4>John Schifferli</h4>
						<span class="about__title">Director</span>
						<a class="about__email" href="mailto:john.schifferli@elevatordirection.com.au">
							<i class="icon-ed icon-ed--call">
								<svg version="1.2" preserveAspectRatio="none" viewBox="0 0 24 24"><g><path xmlns:default="http://www.w3.org/2000/svg" d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H4V8l8 5 8-5v10zm-8-7L4 6h16l-8 5z"></path></g></svg>
							</i>Email</a>
						<a class="about__contactno" href="tel:+61406676036">
							<i class="icon-ed icon-ed--call">
								<svg viewBox="0 0 110 110" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<g id="icon-call" fill="#000000" fill-rule="nonzero">
											<path d="M80.6666667,66 C73.3333333,73.3333333 73.3333333,80.6666667 66,80.6666667 C58.6666667,80.6666667 51.3333333,73.3333333 44,66 C36.6666667,58.6666667 29.3333333,51.3333333 29.3333333,44 C29.3333333,36.6666667 36.6666667,36.6666667 44,29.3333333 C51.3333333,22 29.3333333,0 22,0 C14.6666667,0 0,22 0,22 C0,36.6666667 15.0663333,66.3996667 29.3333333,80.6666667 C43.6003333,94.9336667 73.3333333,110 88,110 C88,110 110,95.3333333 110,88 C110,80.6666667 88,58.6666667 80.6666667,66 Z" id="Shape"></path>
										</g>
									</g>
								</svg>
							</i>0406 676 036</a>
						<p>John Schifferli has been working in the lift industry since 1993. His extensive knowledge of all things VT, combined with his project management skills, provides our clients with excellent cost effective outcomes for modernisations, new builds, safety compliance and life cycle planning.</p>
					</div>
					<div class="about__item-image">
							<!-- <img srcset="img/about-John.S.jpg,
														img/about-John.S@2x.jpg 2x" 
										src="img/about-John.S@2x.jpg" alt="Elevator Direction - John Schifferli" /> -->
							<img srcset="img/about2-john-s.jpg,
											img/about2-john-s@2x.jpg 2x" 
							src="img/about2-john-s@2x.jpg" alt="Elevator Direction - John Schifferli" />
					</div>
				</div>
				<div class="about__item">
					<div class="about__item-desc">
						<h4>John Hughes</h4>
						<span class="about__title">Senior Lift Consultant</span>
						<a class="about__email" href="mailto:john.hughes@elevatordirection.com.au">
							<i class="icon-ed icon-ed--call">
								<svg version="1.2" preserveAspectRatio="none" viewBox="0 0 24 24"><g><path xmlns:default="http://www.w3.org/2000/svg" d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H4V8l8 5 8-5v10zm-8-7L4 6h16l-8 5z"></path></g></svg>
							</i>Email</a>
						<a class="about__contactno" href="tel:+61457035143">
							<i class="icon-ed icon-ed--call">
									<svg viewBox="0 0 110 110" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<g id="icon-call" fill="#000000" fill-rule="nonzero">
												<path d="M80.6666667,66 C73.3333333,73.3333333 73.3333333,80.6666667 66,80.6666667 C58.6666667,80.6666667 51.3333333,73.3333333 44,66 C36.6666667,58.6666667 29.3333333,51.3333333 29.3333333,44 C29.3333333,36.6666667 36.6666667,36.6666667 44,29.3333333 C51.3333333,22 29.3333333,0 22,0 C14.6666667,0 0,22 0,22 C0,36.6666667 15.0663333,66.3996667 29.3333333,80.6666667 C43.6003333,94.9336667 73.3333333,110 88,110 C88,110 110,95.3333333 110,88 C110,80.6666667 88,58.6666667 80.6666667,66 Z" id="Shape"></path>
											</g>
										</g>
									</svg>
								</i>0457 035 143</a>
						<p>John has worked in the lift industry for 40 years and brings excellent maintenance orientated skills to our clients ensuring they gain the right advice in their preventative maintenance scope and contracts.</p>
					</div>
					<div class="about__item-image">
							<!-- <img srcset="img/about-John.H.jpg,
														img/about-John.H@2x.jpg 2x" 
										src="img/about-John.H@2x.jpg" alt="Elevator Direction - Alan Hughes" /> -->
						<img srcset="img/about2-john-h.jpg,
							img/about2-john-h@2x.jpg 2x" 
							src="img/about2-john-h@2x.jpg" alt="Elevator Direction - Alan Hughes" />
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-xs-12 col-md-8">
				<h2 class="about__location-header"><span>South Australia</span></h2>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
			<div class="about__item">
					<div class="about__item-desc">
						<h4>Terry Reese</h4>
						<span class="about__title">Lift Consultant</span>
						<a class="about__email" href="mailto:terry.reese@elevatordirection.com.au">
							<i class="icon-ed icon-ed--call">
								<svg version="1.2" preserveAspectRatio="none" viewBox="0 0 24 24"><g><path xmlns:default="http://www.w3.org/2000/svg" d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H4V8l8 5 8-5v10zm-8-7L4 6h16l-8 5z"></path></g></svg>
							</i>Email
						</a>
						<a class="about__contactno" href="tel:0431 861 999">
							<i class="icon-ed icon-ed--call">
									<svg viewBox="0 0 110 110" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<g id="icon-call" fill="#000000" fill-rule="nonzero">
												<path d="M80.6666667,66 C73.3333333,73.3333333 73.3333333,80.6666667 66,80.6666667 C58.6666667,80.6666667 51.3333333,73.3333333 44,66 C36.6666667,58.6666667 29.3333333,51.3333333 29.3333333,44 C29.3333333,36.6666667 36.6666667,36.6666667 44,29.3333333 C51.3333333,22 29.3333333,0 22,0 C14.6666667,0 0,22 0,22 C0,36.6666667 15.0663333,66.3996667 29.3333333,80.6666667 C43.6003333,94.9336667 73.3333333,110 88,110 C88,110 110,95.3333333 110,88 C110,80.6666667 88,58.6666667 80.6666667,66 Z" id="Shape"></path>
											</g>
										</g>
									</svg>
								</i>0431 861 999</a>
						<p>
							Terry Reese commenced his time in the lift industry in 2007. He has compiled valuable experience across construction, modernisation and maintenance. This varied experience has provided Terry with a well-rounded understanding of the lift industry. Terry enjoys finding solutions to complex problems to create win-win situations for all stakeholders.
						</p>
					</div>
					<div class="about__item-image">
						<img srcset="img/about-2-terry.r.jpg,
									img/about-2-terry.r@2x.jpg 2x" 
							src="img/about-2-terry.r@2x.jpg" alt="Elevator Direction - Terry Reese" />
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
			<div class="about__item">
					<div class="about__item-desc">
						<h4>Alan Field</h4>
						<span class="about__title">Senior Lift Consultant</span>
						<a class="about__email" href="mailto:alan.field@elevatordirection.com.au">
							<i class="icon-ed icon-ed--call">
								<svg version="1.2" preserveAspectRatio="none" viewBox="0 0 24 24"><g><path xmlns:default="http://www.w3.org/2000/svg" d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H4V8l8 5 8-5v10zm-8-7L4 6h16l-8 5z"></path></g></svg>
							</i>Email</a>
						<a class="about__contactno" href="tel:0418 257 061">
							<i class="icon-ed icon-ed--call">
									<svg viewBox="0 0 110 110" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<g id="icon-call" fill="#000000" fill-rule="nonzero">
												<path d="M80.6666667,66 C73.3333333,73.3333333 73.3333333,80.6666667 66,80.6666667 C58.6666667,80.6666667 51.3333333,73.3333333 44,66 C36.6666667,58.6666667 29.3333333,51.3333333 29.3333333,44 C29.3333333,36.6666667 36.6666667,36.6666667 44,29.3333333 C51.3333333,22 29.3333333,0 22,0 C14.6666667,0 0,22 0,22 C0,36.6666667 15.0663333,66.3996667 29.3333333,80.6666667 C43.6003333,94.9336667 73.3333333,110 88,110 C88,110 110,95.3333333 110,88 C110,80.6666667 88,58.6666667 80.6666667,66 Z" id="Shape"></path>
											</g>
										</g>
									</svg>
								</i>0418 257 061</a>
						<p>
						Alan Field has been working in the lift industry since 1962. His experience and considerable knowledge in vertical transport, along with his ability to get projects up and running is valuable to our clients.
						</p>
					</div>
					<div class="about__item-image">
						<img srcset="img/about2-alan-f.jpg,
									img/about2-alan-f@2x.jpg 2x" 
							src="img/about2-alan-f@2x.jpg" alt="Elevator Direction - Alan Field" />
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-xs-12 col-md-8">
				<h2 class="about__location-header"><span>Contracts Management</span></h2>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
			<div class="about__item">
					<div class="about__item-desc">
						<h4>Bronwyn Silver</h4>
						<!-- <span class="about__title">Senior Lift Consultant</span> -->
						<a class="about__email" href="mailto:Bronwyn.Silver@elevatordirection.com.au">
							<i class="icon-ed icon-ed--call">
								<svg version="1.2" preserveAspectRatio="none" viewBox="0 0 24 24"><g><path xmlns:default="http://www.w3.org/2000/svg" d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H4V8l8 5 8-5v10zm-8-7L4 6h16l-8 5z"></path></g></svg>
							</i>Email</a>
						<p>
						Bronwyn Silver has excellent organisational skills and attention to detail which provides our clients with quality contract management services. From setting out original progress cash flows through to certification and completion documentation, recording of site minutes and managing our safety compliance.
						</p>
					</div>
					<div class="about__item-image">
						<img srcset="img/about-2-bronwyn.s.jpg,
									img/about-2-bronwyn.s@2x.jpg 2x" 
							src="img/about-2-bronwyn.s@2x.jpg" alt="Elevator Direction - Bronwyn Silver" />
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
			
		
	</div>

	<?php include_once('includes/footer.php'); ?>
	

	<!-- Javascript
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<?php include_once('includes/js.php'); ?>
</body>

</html>